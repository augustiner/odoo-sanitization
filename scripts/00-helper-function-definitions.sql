-- This script defines functions that can be used in later files. and helps to avoid redunancy

-- Following function can be used to run a query only if table and column exist. If some
-- module is not installed, column and/or table doesn't exist and query doesn't get executed.

create or replace function fn_run_if_column_exists (_column text, _table text, _query text) returns void as 
$$
begin
If exists (select 1 
from information_schema.columns  
where table_schema='public' and table_name=''||_table||'' and column_name=''||_column||'' ) then
execute format(_query);
end if;
end;
$$
language plpgsql;
