-- This is global odoo sanitization script which will always be executed regardless of odoo version.
-- On top of that, each individual odoo version has its own sanitization script located in the separate branch
-- and will be executed after this script

-- When adding a new sanitization query, make sure to properly decide whether the query should be located in
-- the specific odoo version or global sanitization script

-- don't print Notices
SET client_min_messages TO WARNING;

-- change odoo db uuid
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
UPDATE ir_config_parameter SET value = uuid_generate_v4() WHERE key = 'database.uuid'
and exists (select 1 from ir_config_parameter where key = 'database.uuid');

-- set web.base.url to localhost URL
update ir_config_parameter set value = 'https://localhost:8069' where key = 'web.base.url';

-- set report.url to localhost URL if exists
update ir_config_parameter set value = 'http://0.0.0.0:8069'
where key = 'report.url'
and exists (select 1 from ir_config_parameter where key = 'report.url');

-- disable cron jobs
update ir_cron set active = false;

-- disable mail server and change server parameters to prevent ondemand email send actions
-- Jay/II-19 
-- update ir_mail_server set active = false;
update ir_mail_server set smtp_host = smtp_host || '-test';
update ir_mail_server set smtp_user  = smtp_user || '-test';

-- disable fetchmail server and change server URL to prevent ondemand email fetch actions
SELECT fn_run_if_column_exists('active', 'fetchmail_server', 'update fetchmail_server set active = False');
SELECT fn_run_if_column_exists('active', 'fetchmail_server', $$update fetchmail_server set server = server || '-test'$$);
SELECT fn_run_if_column_exists('active', 'fetchmail_server', $$update fetchmail_server set password = password || '-test'$$);

