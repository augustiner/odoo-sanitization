# Odoo Sanitization scripts

Scripts inside `script` directory are being run by default at the beginning of the sanitization process.
Order of execution is by name of the file.

### Important note: Branches match version of Odoo